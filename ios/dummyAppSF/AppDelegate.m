/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import "AppDelegate.h"

#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <MarketingCloudSDK/MarketingCloudSDK.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  
  //salesforce config start
  MarketingCloudSDKConfigBuilder *mcsdkBuilder = [MarketingCloudSDKConfigBuilder new];
  [mcsdkBuilder sfmc_setApplicationId:@"530c3d45-c1fb-43de-8b8d-5e0ddd148b38"];
  [mcsdkBuilder sfmc_setAccessToken:@"LLmAJNV1T502GI8qvqljqWQh"];
  [mcsdkBuilder sfmc_setAnalyticsEnabled:@(YES)];
  [mcsdkBuilder sfmc_setMarketingCloudServerUrl:@"https://mcsdjsrw8dfn6dly2qkc0l3vqvpm.device.marketingcloudapis.com/"];

  NSError *error = nil;
  BOOL success =
      [[MarketingCloudSDK sharedInstance] sfmc_configureWithDictionary:[mcsdkBuilder sfmc_build]
                                                                 error:&error];
  
  //salesforce registration start
  if (success == YES) {
          dispatch_async(dispatch_get_main_queue(), ^{
            if (@available(iOS 10, *)) {
                // set the UNUserNotificationCenter delegate - the delegate must be set here in
                // didFinishLaunchingWithOptions
                [UNUserNotificationCenter currentNotificationCenter].delegate = self;
                [[UIApplication sharedApplication] registerForRemoteNotifications];

                [[UNUserNotificationCenter currentNotificationCenter]
                    requestAuthorizationWithOptions:UNAuthorizationOptionAlert |
                                                    UNAuthorizationOptionSound |
                                                    UNAuthorizationOptionBadge
                                  completionHandler:^(BOOL granted, NSError *_Nullable error) {
                                    if (error == nil) {
                                        if (granted == YES) {
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                           });
                                        }
                                    }
                                  }];
            } else {
  #if __IPHONE_OS_VERSION_MIN_REQUIRED < 100000
                UIUserNotificationSettings *settings = [UIUserNotificationSettings
                    settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeSound |
                                     UIUserNotificationTypeAlert
                          categories:nil];
                [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
  #endif
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
          });
      } else {
          //  MarketingCloudSDK sfmc_configure failed
          os_log_debug(OS_LOG_DEFAULT, "MarketingCloudSDK sfmc_configure failed with error = %@",
                       error);
      }
  //salesforce registration end
  
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"dummyAppSF"
                                            initialProperties:nil];

  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}



- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}

@end
