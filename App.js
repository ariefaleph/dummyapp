
import React, { Component } from 'react';
import {
  StyleSheet,
  Button,
  View,
  SafeAreaView,
  Text,
  TextInput,
  Alert,
  Clipboard,
  ScrollView
} from 'react-native';

import MCReactModule from 'react-native-marketingcloudsdk';

import axios from 'axios';

function Separator() {
  return <View style={styles.separator} />;
}

export default class TestApp extends Component {

  constructor(props) {
    super(props);
    this.state = {text: '',
                  contactKey: '',
                  accessToken : '',
                  fullname : ''};
  }

  componentDidMount(){
    
    let togglePromise = MCReactModule.enablePush();
    this.updatePushData();
  }

  async updatePushData() {
    let enabled = await MCReactModule.isPushEnabled();
    Alert.alert("push status : " + enabled);
  }

  setContactKeyButton(){
    console.log('Register Button pressed test');
    let togglePromise = MCReactModule.enablePush();
    this.setContactKey();
  }

  setContactKey() {
    MCReactModule.setContactKey(this.state.text);
    MCReactModule.setAttribute("ByUregistrationstatus", "true");
    MCReactModule.setAttribute("ByUEmailAddress", this.state.text);
    this.updateContactKey();
  }
  
  async updateContactKey() {
    let contactKeyGet = await MCReactModule.getContactKey();
    this.setState({
        contactKey: contactKeyGet,
    });
    Alert.alert('get contact key value (state): ' + this.state.contactKey);
  }

  /*getToken(){

    let tempCK = this.state.contactKey;

    axios.post("https://mcsdjsrw8dfn6dly2qkc0l3vqvpm.auth.marketingcloudapis.com/v2/token", {
      grant_type: "client_credentials",
      client_id: "ss4mc9lqd5ucnrfht5m3my14",
      client_secret: "XVRrASUzlzAKTUceRnJBhPGh",
      account_id: "514002223"
      })
    .then(function (response) {
      console.warn("test cek response : ", response);
      let tempToken = response.data.access_token;
      let allToken = 'Bearer ' + tempToken;
      console.log("token generated : " + tempToken);
      axios.post("https://mcsdjsrw8dfn6dly2qkc0l3vqvpm.rest.marketingcloudapis.com/interaction/v1/events",{
        ContactKey: tempCK,
        EventDefinitionKey:"APIEvent-4333ff4d-8dc4-529c-36c1-d457dc0ee542"
      },{
        headers: {
          'Content-Type': 'application/json',
          'Authorization' : allToken
        }
      })
      .then(function(response2){
        console.warn("response generated : " + JSON.stringify(response2));
        Alert.alert("response generated : " + JSON.stringify(response2));
      })
      .catch(function(error2){
        console.log(error2);
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  }*/

  getUC3NotifName(){

    let tempCK = this.state.contactKey;
    let nameInsert = this.state.fullname;

    axios.post("https://mcsdjsrw8dfn6dly2qkc0l3vqvpm.auth.marketingcloudapis.com/v2/token", {
      grant_type: "client_credentials",
      client_id: "zvhbo86f85q9v8qb4plv5x8r",
      client_secret: "vKhr5mmrjSyucmzQNt656g0a",
      account_id: "514003194"
      })
    .then(function (response) {
      console.warn("test cek response : ", response);
      let tempToken = response.data.access_token;
      let allToken = 'Bearer ' + tempToken;
      console.warn("nameinsert : " + nameInsert);
      console.log("token generated : " + tempToken);
      axios.post("https://mcsdjsrw8dfn6dly2qkc0l3vqvpm.rest.marketingcloudapis.com/interaction/v1/events",{
        ContactKey: tempCK,
        EventDefinitionKey: "APIEvent-f3d63a29-7970-1a43-46ff-5286e8e9d83b",
        Data: {
          'FIRST_NAME' : nameInsert
        }
      },{
        headers: {
          'Content-Type': 'application/json',
          'Authorization' : allToken
        }
      })
      .then(function(response2){
        this.setState({ jouneyKey: response2.data.eventInstanceId })
        console.log("token generated : " + JSON.stringify(response2));
        alert(JSON.stringify({
          ContactKey: tempCK,
          FIRST_NAME: nameInsert,
          msg: "Journey Success",
          data: response2
        }))
      })
      .catch(function(error2){
        alert(JSON.stringify({
          msg: "Error Journey",
          data: error2
        }))
      })
    })
    .catch(function (error) {
      alert(JSON.stringify({
        msg: "Error Token",
        data: error
      }))
    });
  }

  render(){
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <Text style={styles.title}>
            Set Contact Key Value
          </Text>
          <TextInput
            style={{height: 40}}
            placeholder="Type here to set contact key"
            onChangeText={(text) => this.setState({text})}
            value={this.state.text}
          />
          <TextInput
            style={{height: 40}}
            placeholder="Type here to set first name"
            onChangeText={(fullname) => this.setState({fullname})}
            value={this.state.fullname}
          />
          <Button
            title="Set Values"
            onPress={() => {
              Alert.alert('Contact Key + First Name! Set : ' + this.state.text + ',' + this.state.fullname);
            }}
          />
        </View>
        <Separator />
        <View>
          <Text style={styles.title}>
            Try to Register App
          </Text>
          <Button
            title="Register App"
            onPress={() => {
              this.setContactKeyButton()
            }}
          />
        </View>
        <Separator />
        <View>
          <Text style={styles.title}>
            Trigger Journey UC3 with Name Notif
          </Text>
          <Button
            title="Trigger Journey"
            onPress={() => this.getUC3NotifName()}
          />
        </View>
        <Separator />
        <View>
          <Text style={styles.title2}>
            grant_type: client_credentials
          </Text>
          <Text style={styles.title2}>
          client_id: zvhbo86f85q9v8qb4plv5x8r
          </Text>
          <Text style={styles.title2}>
          client_secret: vKhr5mmrjSyucmzQNt656g0a
          </Text>
          <Text style={styles.title2}>
          account_id: 514003194
          </Text>
          <Text style={styles.title2}>
          EventDefinitionKey: APIEvent-f3d63a29-7970-1a43-46ff-5286e8e9d83b
          </Text>
          <Separator />
          <Text style={styles.title2}>
          sfmc_setApplicationId: 530c3d45-c1fb-43de-8b8d-5e0ddd148b38
          </Text>
          <Text style={styles.title2}>
          sfmc_setAccessToken: LLmAJNV1T502GI8qvqljqWQh
          </Text>
          <Text style={styles.title2}>
          sfmc_setMarketingCloudServerUrl: https://mcsdjsrw8dfn6dly2qkc0l3vqvpm.device.marketingcloudapis.com/
          </Text>
          <Separator />
          <Text style={styles.title2}>
           Email: {this.state.contactKey}
          </Text>
          <Text style={styles.title2}>
           FirstName: {this.state.fullname}
          </Text>
          <Text style={styles.title2} onPress={() => {
            Clipboard.setString(String(this.state.jouneyKey))
            alert("Copy success")
          }}>
           Journey Key (Copy): {this.state.jouneyKey}
          </Text>
        </View>
        </ScrollView>
      </SafeAreaView>
    );
  }

  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    
    marginHorizontal: 16,
  },
  title: {
    textAlign: 'center',
    marginVertical: 8,
  },
  title2: {
    textAlign: 'left',
    marginVertical: 8,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});